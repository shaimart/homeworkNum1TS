import org.example.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void sumTest(){

        //act
        int sum = calculator.add(2,2);
        //assert
        Assertions.assertEquals(4, sum);
    }
    @Test
    void subtractTest(){


        //act
        int subtract = calculator.subtract(2,2);
        //assert
        Assertions.assertEquals(0, subtract);
    }
    @Test
    void multiplyTest(){

        //act
        int multiply = calculator.multiply(2,2);
        //assert
        Assertions.assertEquals(4, multiply);
    }
    @Test
    void divideTest(){
        //act
        int divide = calculator.divide(2,2);
        //assert
        Assertions.assertEquals(1, divide);
    }
    @Test
    void divide_byZERO_Test(){
        //assert
        Assertions.assertThrows(ArithmeticException.class ,
                () -> calculator.divide(2,0));
    }

}
