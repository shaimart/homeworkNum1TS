package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    @Test
    void testConstructor() {
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, "Ivan", "Moskva, Tveskaya 9, 95");
        assertEquals("Ivan", order.getCustomerName());
        assertEquals("Moskva, Tveskaya 9, 95", order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    @Test
    void testConstructorWithNullValues() {
        Order order = new Order(null, null, null);
        assertNull(order.getCustomerName());
        assertNull(order.getCustomerAddress());
        assertEquals(0, order.getState());
    }
}