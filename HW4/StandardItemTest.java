package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    @Test
    void testConstructor() {
        StandardItem item = new StandardItem(1, "Banana", 20.0f, "Fruits", 16);
        assertEquals(1, item.getID());
        assertEquals("Banana", item.getName());
        assertEquals(20.0f, item.getPrice());
        assertEquals("Fruits", item.getCategory());
        assertEquals(16, item.getLoyaltyPoints());
    }

    @Test
    void testCopy() {
        StandardItem item = new StandardItem(1, "Kiwi", 30.0f, "Fruits", 2);
        StandardItem copy = item.copy();
        assertEquals(item, copy);
        assertNotSame(item, copy);
    }

    @Test
    void testEquals() {
        StandardItem item1 = new StandardItem(1, "Banana", 100.0f, "Fruits", 10);
        StandardItem item2 = new StandardItem(1, "Banana", 100.0f, "Fruits", 10);
        StandardItem item3 = new StandardItem(2, "Item", 100.0f, "Category", 10);
        assertTrue(item1.equals(item2));
        assertFalse(item1.equals(item3));
    }
}