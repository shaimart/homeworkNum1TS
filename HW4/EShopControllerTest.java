package cz.cvut.fel.ts1.shop;
import static org.mockito.Mockito.*;

import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.storage.NoItemInStorage;
import cz.cvut.fel.ts1.storage.Storage;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class EShopControllerTest {

    @Test
    void testPurchaseShoppingCart() throws NoItemInStorage {
        EShopController.startEShop();
        ShoppingCart newCart = EShopController.newCart();
        Item item = new StandardItem(1, "Test Item", 1000, "GADGETS", 5);
        newCart.addItem(item);
        assertThrows(NoItemInStorage.class, () -> {
            EShopController.purchaseShoppingCart(newCart, "Test Customer", "Test Address");
        });
    }

    @Test
     void testPurchaseEmptyShoppingCart() {
        EShopController.startEShop();
        ShoppingCart newEmptyCart = EShopController.newCart();
        assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(newEmptyCart, "Test Customer", "Test Address"));
    }
}