package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {
    @Test
    void testConstructor() {
        Item item = new StandardItem(1, "Apple", 55.0f, "Fruits", 100);
        ItemStock stock = new ItemStock(item);
        assertEquals(item, stock.getItem());
        assertEquals(0, stock.getCount());
    }

    @Test
    void testIncreaseItemCount() {
        Item item = new StandardItem(1, "Beer", 100.0f, "Alkohol", 1);
        ItemStock stock = new ItemStock(item);
        stock.IncreaseItemCount(1);
        assertEquals(1, stock.getCount());
    }

    @Test
    void testDecreaseItemCount() {
        Item item = new StandardItem(1, "Bread", 30.0f, "Staple food", 10);
        ItemStock stock = new ItemStock(item);
        stock.IncreaseItemCount(10);
        stock.decreaseItemCount(5);
        assertEquals(5, stock.getCount());
    }
}