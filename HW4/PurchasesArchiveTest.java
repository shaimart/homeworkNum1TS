package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class PurchasesArchiveTest {

    @Test
    void printItemPurchaseStatisticsTest() {
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        itemArchive.put(1, entry);
        ArrayList<Order> orderArchive = new ArrayList<>();
        PurchasesArchive archive = new PurchasesArchive(itemArchive, orderArchive);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        archive.printItemPurchaseStatistics();
        String expectedOutput = "ITEM PURCHASE STATISTICS:\n" + entry.toString() + "\n";
        assertEquals(expectedOutput, outContent.toString());
        System.setOut(System.out);
    }

    @Test
    void getHowManyTimesHasBeenItemSoldTest() {
        Item item = new StandardItem(1, "Item", 100.0f, "Category", 10);
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        when(entry.getCountHowManyTimesHasBeenSold()).thenReturn(5);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(1, entry);
        ArrayList<Order> orderArchive = new ArrayList<>();
        PurchasesArchive archive = new PurchasesArchive(itemArchive, orderArchive);
        int count = archive.getHowManyTimesHasBeenItemSold(item);
        assertEquals(5, count);
    }

    @Test
    void putOrderToPurchasesArchiveTest() {
        ShoppingCart cart = mock(ShoppingCart.class);
        StandardItem item = new StandardItem(1, "Water", 100.0f, "Drinks", 10);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        when(cart.getCartItems()).thenReturn(items);
        Order order = new Order(cart, "Ivan", "Moskva, Tverskaya 9, 95");
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        itemArchive.put(1, entry);
        PurchasesArchive archive = new PurchasesArchive(itemArchive, new ArrayList<>());
        archive.putOrderToPurchasesArchive(order);
        verify(entry, times(1)).increaseCountHowManyTimesHasBeenSold(1);
    }
}