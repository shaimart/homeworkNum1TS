import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class facTest {
    @Test
    public void factorialTest() {
        assertEquals(1, fac.factorial(0));
        assertEquals(1, fac.factorial(1));
        assertEquals(2, fac.factorial(2));
        assertEquals(6, fac.factorial(3));
        assertEquals(24, fac.factorial(4));
        assertEquals(120, fac.factorial(5));
    }

}
